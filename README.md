# Usage
```
usage: PDF reconstructor for two sided papers where the scanning is done in two passes where the first pass scans odd pages front to back, and the second pass scans even pages back to front.
       [-h] --output OUTPUT odd_pages even_pages

positional arguments:
  odd_pages             Give a pdf of odd pages where the odd pages are scanned from page 1 to the
                        end.
  even_pages            Give a pdf of even pages where the even pages are scanned from the last page
                        to the first.

options:
  -h, --help            show this help message and exit
  --output OUTPUT, -o OUTPUT
```

# Example:
```
$ python3 main.py odd_pages.pdf even_pages.pdf -o output.pdf
```
