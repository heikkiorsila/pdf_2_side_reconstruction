import argparse
import subprocess
import tempfile


def _get_num_pages(fname: str):
    cp = subprocess.run(['qpdf', '--show-npages', '--', fname],
                        capture_output=True)
    try:
        return int(cp.stdout.decode())
    except ValueError:
        print('Can not obtain number of pages for {}'.format(fname))
        raise


def main():
    parser = argparse.ArgumentParser(
        'PDF reconstructor for two sided papers where the scanning is done in '
        'two passes where the first pass scans odd pages front to back, and '
        'the second pass scans even pages back to front.')
    parser.add_argument(
        'odd_pages',
        help=('Give a pdf of odd pages where the odd pages are scanned from '
              'page 1 to the end.'))
    parser.add_argument(
        'even_pages',
        help=('Give a pdf of even pages where the even pages are scanned from '
              'the last page to the first.'))
    parser.add_argument('--output', '-o', required=True)
    args = parser.parse_args()

    npages = []
    for pdf in (args.odd_pages, args.even_pages):
        npages.append(_get_num_pages(pdf))

    if not (npages[0] == npages[1] or npages[0] == (npages[1] + 1)):
        print()
        print('Warning: There are {} odd pages and {} even pages. Some of the '
              'pages are lost.'.format(npages[0], npages[1]))
        print()

    with tempfile.NamedTemporaryFile(mode='r+b', suffix='.pdf') as f:
        tname = f.name
        concat_cmd = ['qpdf', args.even_pages, '--pages',
                      args.odd_pages, '1-z', args.even_pages, '1-z', '--',
                      tname]
        print('Running:', concat_cmd)
        cp = subprocess.run(concat_cmd)
        if cp.returncode != 0:
            print('Concatenating documents failed:', concat_cmd)
            return 1
        odd_page = 1
        even_page = npages[0] + npages[1]
        pages = []

        # Note: we try to reconstruct incomplete documents where some of the
        #       odd or even pages are missing.
        while odd_page <= npages[0] or even_page > npages[0]:
            if odd_page <= npages[0]:
                pages.append(str(odd_page))
                odd_page += 1
            if even_page > npages[0]:
                pages.append(str(even_page))
                even_page -= 1

        page_range = ','.join(pages)

        reorder_cmd = ['qpdf', tname, '--pages', tname, page_range, '--',
                       args.output]
        print('Running:', reorder_cmd)
        cp = subprocess.run(reorder_cmd)
        if cp.returncode != 0:
            print('Reordering pages failed:', reorder_cmd)
            return 1


if __name__ == '__main__':
    main()
